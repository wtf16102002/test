# Cleartext Transmission of Session Token

<hr>

## Noncompliant code

***Example 1: Description***

``` <language>
abc
```

Description about noncomplaint code

***Example 2: Description***

``` <language>
abc
```

Description about noncomplaint code

## Compliant code

<details>
<summary>If each example has a solution, use this template</summary>

***Example 1: Description***

``` <language>
def
```

Description about complaint code
</details>

<details>
<summary>If all the examples have one solution, use this template</summary>

``` <language>
def
```

Description about complaint code
</details>

<details>
<summary>If one example has many solution, use this template</summary>

***Example 1: Description***

- **Solution 1: Description**
    ``` <language>
    def
    ```
    Description about complaint code

- **Solution 2: Description**
    ``` <language>
    def
    ```
    Description about complaint code

</details>